package thirdLoggingSwing;

import javax.swing.*;
import java.awt.*;

public class WrongPasswordDialog {
    private JDialog dialog;
    private Label wrong;
    private JButton button;

    public WrongPasswordDialog() {
        dialog = new JDialog();
        dialog.setLayout(null);
        dialog.setBounds(300, 300, 400, 450);
        dialog.setLayout(null);

        wrong = new Label("Wrong");
        wrong.setBounds(0, 0, 400, 200);
        dialog.add(wrong);

        button = new JButton("OK");
        button.setBounds(0, 200, 400, 200);
        button.addActionListener((event) -> hideDialog());
        dialog.add(button);
    }

    public void showDialog() {
        dialog.setVisible(true);

    }

    public void hideDialog() {
        dialog.setVisible(false);
    }
}
