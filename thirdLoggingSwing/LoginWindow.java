package thirdLoggingSwing;

import javax.swing.*;
import java.awt.*;

public class LoginWindow {

    private JFrame frame;
    private JTextField textField;
    private JButton button;
    private WrongPasswordDialog wrongPasswordDialog;
    private SecretDialog secretDialog;

    public LoginWindow() {
        wrongPasswordDialog = new WrongPasswordDialog();
        secretDialog = new SecretDialog();

        frame = new JFrame("LoginWindow");
        frame.setLayout(null);
        frame.setBounds(100, 100, 400, 450);

        textField = new JTextField("Password");
        textField.setBounds(0, 0, 400, 200);
        textField.setBackground(Color.pink);
        frame.add(textField);

        button = new JButton("LOGIN");
        button.setBounds(0, 200, 400, 200);
        button.setBackground(Color.blue);
        button.setFont(new Font (null, Font.BOLD | Font.ITALIC, 50));
        frame.add(button);
        button.addActionListener(e -> {
            Password();
        });
    }

    private void Password(){
        if (textField.getText().equals("zoo")) {
            secretDialog.showSecretDialog();
            hideFrame();
        } else {
            wrongPasswordDialog.showDialog();
        }
    }

    public void showFrame(){
        frame.setVisible(true);
    }

    public void hideFrame() {
        frame.setVisible(false);
    }
}
